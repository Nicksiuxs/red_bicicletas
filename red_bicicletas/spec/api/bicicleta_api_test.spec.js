let Bicicleta = require("../../models/Bicicleta");
let mongoose = require("mongoose");
let request = require("request");
let server = require("../../bin/www");

let base_url = "http://localhost:3000/api/bicicletas";

describe("Bicicleta api", () => {
  beforeEach(function (done) {
    let mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", function () {
      console.log("we are connected to test database");
      done();
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, sucess) {
      if (err) console.log(err);
      done();
    });
  });

  describe("GET BICICLETAS /", () => {
    it("Status 200", (done) => {
      request.get(base_url, function (error, response, body) {
        let result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(response.bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe("POST BICICLETAS /create", () => {
    it("STATUS 200", (done) => {
      let headers = { "content-type": "application/json" };
      let aBici = `{"id":10, "color":"rojo", "modelo":"urbana", "lat":-34, "lng":-54}`;

      request.post(
        {
          headers: headers,
          url: base_url + "/create",
          body: aBici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          let bici = JSON.parse(body).Bicicleta;
          console.log(bici);
          expect(bici.color).toBe("rojo");
          expect(bici.ubicacion[0]).toBe(-34);
          expect(bici.ubicacion[1]).toBe(-54);
          done();
        }
      );
    });
  });

  // describe("DELETE BICICLETA /delete",(done)=>{

  // })
});
