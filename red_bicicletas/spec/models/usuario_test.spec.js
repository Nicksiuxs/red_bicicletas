let mongoose = require("mongoose");
let Bicicleta = require("../../models/Bicicleta");
let Usuario = require("../../models/Usuario");
let Reserva = require("../../models/Reserva");

describe("Testing Usuarios", function () {
  beforeEach(function (done) {
    let mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", function () {
      console.log("we are connected to test database");
      done();
    });
  });

  afterEach(function (done) {
    Reserva.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      Usuario.deleteMany({}, function (err, success) {
        if (err) console.log(err);
        Bicicleta.deleteMany({}, function (err, success) {
          if (err) console.log(err);
          done();
        });
      });
    });
  });

  describe("Cuando un usuario reserva una bici", () => {
    it("debe existir la reserva", (done) => {
      const usuario = new Usuario({ nombre: "Nicolas" });
      usuario.save();
      const bicicleta = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urba",
      });
      bicicleta.save();

      let hoy = new Date();
      let manana = new Date();
      manana.setDate(hoy.getDate() + 1);
      usuario.reservar(bicicleta.id, hoy, manana, function (err, reserva) {
        Reserva.find({})
          .populate("bicicleta")
          .populate("usuario")
          .exec(function (err, reservas) {
            console.log(reservas[0]);
            expect(reservas.length).toBe(1);
            expect(reservas[0].diasDeReserva()).toBe(2);
            expect(reservas[0].bicicleta.code).toBe(1);
            expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
            done();
          });
      });
    });
  });
});
