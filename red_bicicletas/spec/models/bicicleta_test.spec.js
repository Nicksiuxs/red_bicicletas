let mongoose = require("mongoose");
let Bicicleta = require("../../models/Bicicleta");

describe("Testing Bicicletas", function () {
  beforeEach(function (done) {
    let mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", function () {
      console.log("We are conected to test database");
      done();
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("Bicicleta.createInstance", () => {
    it("crea un instancia de Bicicleta", () => {
      let bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, 54.1]);
      expect(bici.code).toBe(1);
      expect(bici.color).toBe("verde");
      expect(bici.modelo).toBe("urbana");
      expect(bici.ubicacion[0]).toEqual(-34.5);
      expect(bici.ubicacion[1]).toEqual(54.1);
    });
  });

  describe("Bicicletas.allBicis", () => {
    it("comienza vacia", (done) => {
      Bicicleta.allBicicletas(function (err, bicis) {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe("Bicicleta,add", () => {
    it("agrega solo una bici", (done) => {
      let aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
      Bicicleta.add(aBici, function (err, newBici) {
        if (err) console.log(err);
        Bicicleta.allBicicletas(function (err, bicis) {
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);

          done();
        });
      });
    });
  });

  describe("Bicicleta.findByCode", () => {
    it("debe devolver  la bici con code 1", (done) => {
      Bicicleta.allBicicletas(function (err, bicis) {
        expect(bicis.length).toBe(0);

        let aBici = new Bicicleta({
          code: 1,
          color: "verde",
          modelo: "urbana",
        });
        Bicicleta.add(aBici, function (err, newBici) {
          if (err) console.log(err);
          Bicicleta.findByCode(1, function (error, targetBici) {
            expect(targetBici.code).toBe(aBici.code);
            expect(targetBici.color).toBe(aBici.color);
            expect(targetBici.modelo).toBe(aBici.modelo);

            done();
          });
        });
      });
    });
  });
});

// beforeEach(() => (Bicicleta.allBicicletas = []));

// describe("Bicileta.allBicicletas", () => {
//   it("comienza vacio ", () => {
//     expect(Bicicleta.allBicicletas.length).toBe(0);
//   });
// });

// describe("Bicicleta.add", () => {
//   it("agregamos una bicicleta", () => {
//     expect(Bicicleta.allBicicletas.length).toBe(0);

//     let a = new Bicicleta(1, "roja", "bmx", [51.498497, -0.104792]);
//     Bicicleta.add(a);

//     expect(Bicicleta.allBicicletas.length).toBe(1);
//     expect(Bicicleta.allBicicletas[0]).toEqual(a);
//   });
// });

// describe("Bicicleta.findById", () => {
//   it("Debe devolver la bici con id 1", () => {
//     expect(Bicicleta.allBicicletas.length).toBe(0);
//     let a = new Bicicleta(1, "roja", "bmx", [51.498497, -0.104792]);
//     let b = new Bicicleta(2, "roja", "bmx", [51.498497, -0.104792]);

//     Bicicleta.add(a);
//     Bicicleta.add(b);

//     let targetBici = Bicicleta.findById(1);

//     expect(targetBici.id).toBe(1);
//     expect(targetBici.color).toEqual(a.color);
//     expect(targetBici.modelo).toEqual(a.modelo);
//   });
// });

// describe("Bicicleta.removeById", () => {
//   it("Debe devolver el tamaño del arreglo menos uno", () => {
//     let a = new Bicicleta(1, "roja", "bmx", [51.498497, -0.104792]);
//     Bicicleta.add(a);

//     Bicicleta.removeById(a.id);
//     expect(Bicicleta.allBicicletas.length).toBe(0);
//   });
// });
