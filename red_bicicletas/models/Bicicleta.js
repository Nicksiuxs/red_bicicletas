let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: { type: "2dsphere", sparse: true },
  },
});

bicicletaSchema.methods.toString = function () {
  `code: ${this.code} color: ${this.color}`;
};

bicicletaSchema.statics.allBicicletas = function (cb) {
  return this.find({}, cb);
};

bicicletaSchema.statics.createInstance = function (
  code,
  color,
  modelo,
  ubicacion
) {
  return new this({
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion,
  });
};

bicicletaSchema.statics.add = function (aBici, cb) {
  this.create(aBici, cb);
};

bicicletaSchema.statics.findByCode = function (aCode, cb) {
  return this.findOne({ code: aCode }, cb);
};

bicicletaSchema.statics.removeByCode = function (aCode, cb) {
  return this.deleteOne({ code: aCode }, cb);
};

module.exports = mongoose.model("Bicicleta", bicicletaSchema);

// let Bicicleta = function (id, color, modelo, ubicacion) {
//   this.id = id;
//   this.color = color;
//   this.modelo = modelo;
//   this.ubicacion = ubicacion;
// };

// Bicicleta.toString = function () {
//   return `id: ${this.id} color: ${this.color} modelo: ${this.model} ubicación: ${this.ubicacion}`;
// };

// Bicicleta.allBicicletas = [];

// Bicicleta.add = (aBici) => {
//   Bicicleta.allBicicletas.push(aBici);
// };

// Bicicleta.findById = (aBiciId) => {
//   let aBici = Bicicleta.allBicicletas.find((x) => x.id == aBiciId);
//   console.log(aBici);
//   if (aBici) return aBici;
//   else
//     throw new Error(`No existe una bicicleta con el id: ${aBiciId} solicitado`);
// };

// Bicicleta.removeById = (aBiciId) => {
//   for (let i = 0; i < Bicicleta.allBicicletas.length; i++) {
//     if (Bicicleta.allBicicletas[i].id == aBiciId) {
//       Bicicleta.allBicicletas.splice(i, 1);
//       break;
//     }
//   }
// };

// let a = new Bicicleta(1, "roja", "bmx", [51.498497, -0.104792]);
// let b = new Bicicleta(2, "azul", "bmx", [51.49858, -0.104457]);

// Bicicleta.add(a);
// Bicicleta.add(b);

// module.exports = Bicicleta;
