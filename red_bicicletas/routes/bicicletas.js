let express = require("express");
let router = express.Router();
let bicicletaController = require("../controllers/BicicletaController");

router.get("/", bicicletaController.Bicicleta_list);
//Create
router.get("/create", bicicletaController.Bicicleta_create_get);
router.post("/create", bicicletaController.Bicicleta_create_post);
//Delete
router.post("/:id/delete", bicicletaController.Bicicleta_delete_post);
//Update
router.get("/:id/update", bicicletaController.Bicicleta_update_get);
router.post("/:id/update", bicicletaController.Bicicleta_update_post);
//show
router.get("/:id/show", bicicletaController.Bicicleta_show_get);

module.exports = router;
