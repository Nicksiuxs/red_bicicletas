let express = require("express");
let router = express.Router();
let bicicletaController = require("../../controllers/api/BicicletaControllerAPI");

router.get("/", bicicletaController.bicicleta_list);
//create
router.post("/create", bicicletaController.bicicleta_create);
//delete
router.delete("/delete", bicicletaController.bicicleta_delete);
//update
router.put("/:id/update", bicicletaController.bicicleta_update);
module.exports = router;
