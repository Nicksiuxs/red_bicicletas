let Bicicleta = require("../../models/Bicicleta");

exports.bicicleta_list = (req, res) => {
  Bicicleta.find({}, function (err, bicicletas) {
    res.status(200).json({
      bicicletas: bicicletas,
    });
  });
};

exports.bicicleta_create = (req, res) => {
  let bici = new Bicicleta({ color: req.body.color, modelo: req.body.modelo });
  bici.ubicacion = [req.body.lat, req.body.lon];

  Bicicleta.add(bici);
  res.status(200).json({
    bicileta: bici,
  });
};

exports.bicicleta_delete = (req, res) => {
  Bicicleta.removeById(req.body.id);
  res.status(204).send();
};

exports.bicicleta_update = (req, res) => {
  let { id, color, modelo, lat, lon } = req.body;
  let bici = Bicicleta.findById(id);
  bici.id = id;
  bici.color = color;
  bici.modelo = modelo;
  bici.ubicacion = [lat, lon];

  res.status(200).json({
    bicicleta: bici,
  });
};
